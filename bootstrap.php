<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-06
 * Time: 16:48
 */
use TPCore\Bootstrap;

require __DIR__ . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

Bootstrap::init(
    __DIR__,
    __DIR__ . DIRECTORY_SEPARATOR . 'tmp'
);

/*
 * Включаю отображение ошибок
 */
if (getenv('SHOW_ERRORS_MODE') === '1') {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}
