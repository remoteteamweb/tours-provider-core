<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-10
 * Time: 22:38
 */
namespace TPCore\Tools\Vk;

use TPCore\Tools\Curl;
use VK\Client\VKApiClient;

class WallPhoto
{
    /**
     * @var array
     */
    protected $saved_photos = [];

    public function __construct(VKApiClient $vk_client, AccessToken $access_token, int $group_id, array $photos)
    {
        $upload_server = $vk_client->photos()->getWallUploadServer((string) $access_token, [
            'group_id' => $group_id,
        ]);

        /*
         * Удаляем из путей нечитаемые фотографии
         */
        $photos = array_filter(array_map(function ($path_to_photo) {
            return is_readable($path_to_photo) ? $path_to_photo : false;
        }, $photos));

        /*
         * Формируем из фотографий данные для загрузки на сервер
         */
        $photos = array_reduce($photos, function ($memo, $path) {
            static $counter = 0;
            $memo['file' . ++$counter] = new \CURLFile($path);

            return $memo;
        }, []);

        $curl = new Curl();
        $curl->post($upload_server['upload_url'], $photos);
        $curl->response = json_decode($curl->response);

        $saved_photo = $vk_client->photos()->saveWallPhoto((string) $access_token, [
            'group_id' => $group_id,
            'server' => $curl->response->server,
            'photo' => $curl->response->photo,
            'hash' => $curl->response->hash,
        ]);

        $this->saved_photos = $saved_photo;
    }

    public function getAttachmentsString()
    {
        return join(',', array_map(function ($item) {
            return sprintf('photo%d_%d', $item['owner_id'], $item['id']);
        }, $this->saved_photos));
    }
}
