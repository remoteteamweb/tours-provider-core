<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-10
 * Time: 14:38
 */
namespace TPCore\Tools\Vk;

class AccessToken
{
    /**
     * @var string
     */
    private $value = '';

    public function __construct(string $token)
    {
        $this->value = $token;
    }

    public function __toString()
    {
        return $this->value;
    }
}
