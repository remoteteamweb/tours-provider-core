<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-09
 * Time: 12:45
 */
namespace TPCore\Tools;

use Campo\UserAgent;

class Curl extends \Curl\Curl
{
    private $user_agent;

    public function __construct($base_url = null)
    {
        parent::__construct($base_url);
        $this->setUserAgent(UserAgent::random());
    }

    public function getUserAgent()
    {
        return $this->user_agent;
    }

    public function setUserAgent($user_agent)
    {
        parent::setUserAgent($user_agent);
        $this->user_agent = $user_agent;
    }
}
