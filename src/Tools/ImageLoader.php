<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 12:44
 */
namespace TPCore\Tools;

class ImageLoader
{
    protected $files = [];

    /**
     * ImageLoader constructor.
     * @param array $urls
     * @param Path $path_to_dir
     * @param bool $strict_mode
     * @throws \ErrorException
     */
    public function __construct(array $urls, Path $path_to_dir, bool $strict_mode = true)
    {
        foreach ($urls as $url) {
            $curl = new Curl();
            $curl->setOpt(CURLOPT_ENCODING, 'gzip');

            $extension = pathinfo($url, PATHINFO_EXTENSION);
            $file_name = pathinfo($url, PATHINFO_FILENAME);

            $path_to_save = (string) $path_to_dir . DIRECTORY_SEPARATOR
                . $file_name . '_'
                . base_convert(md5($url . microtime()), 10, 36)
                . '.' . $extension;

            $curl->download(
                $url,
                $path_to_save
            );

            $image_type = exif_imagetype($path_to_save);
            if ($strict_mode && $image_type === false) {
                unlink($path_to_save);

                throw new \LogicException('Скачанный файл не является изображением: ' . $url);
            } elseif ($image_type === false) {
                continue;
            }

            $this->files[] = $path_to_save;
        }
    }

    public function getFiles():array
    {
        return $this->files;
    }

    public function __destruct()
    {
        foreach ($this->files as $path_to_file) {
            if (!file_exists($path_to_file)) {
                continue;
            }

            unlink($path_to_file);
        }
    }
}
