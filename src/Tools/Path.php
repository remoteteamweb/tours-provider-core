<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 12:18
 */
namespace TPCore\Tools;

class Path
{
    protected $value;

    public static $TMP = 'tmp';

    protected static $defined_paths = [];

    public function __construct(string $path)
    {
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $this->value = $path;
    }

    public function __toString():string
    {
        return $this->value;
    }

    /**
     * create define path
     * @param string $path
     * @return static
     */
    public static function define()
    {
        $args = func_get_args();
        $key = array_shift($args);

        if (!isset(self::$defined_paths[$key])) {
            self::$defined_paths[$key] = new self(...$args);
        }

        return self::$defined_paths[$key];
    }

    public static function tmp()
    {
        return self::$defined_paths[self::$TMP];
    }
}
