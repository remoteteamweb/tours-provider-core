<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 16:53
 */
namespace TPCore;

use Dotenv\Dotenv;
use TPCore\Tools\Path;

class Bootstrap
{
    /**
     * Инициализация ядра приложения
     * @param $dotenv_path string|array
     * @param $tmp_path string
     */
    public static function init($dotenv_path, $tmp_path)
    {
        /*
         * Инициализация .env
         */
        Dotenv::create($dotenv_path)->load();

        /*
         * Инициализация путей
         */
        Path::define(Path::$TMP, $tmp_path);
    }
}
