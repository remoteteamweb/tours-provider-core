<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-10
 * Time: 11:41
 */
namespace TPCore\Exception;

use Throwable;

class Exception extends \Exception
{
    private $extra_data;

    public function __construct(string $message = '', int $code = 0, Throwable $previous = null, array  $extra_data = null)
    {
        parent::__construct($message, $code, $previous);
        $this->extra_data = $extra_data ?: [];
    }

    public function getExtraData()
    {
        return $this->extra_data;
    }
}
