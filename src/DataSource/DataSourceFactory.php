<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-14
 * Time: 12:17
 */
namespace TPCore\DataSource;

use TPCore\DataSource\Sources\OnlineToursParserFactory;

class DataSourceFactory
{
    public function create():SourceFactoryAbstract
    {
        return new OnlineToursParserFactory();
    }
}
