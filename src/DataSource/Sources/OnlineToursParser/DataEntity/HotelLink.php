<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-15
 * Time: 08:08
 */
namespace TPCore\DataSource\Sources\OnlineToursParser\DataEntity;

use TPCore\DataEntity\ItemLinkAbstract;
use TPCore\DataEntity\Offer\Item;

class HotelLink extends ItemLinkAbstract
{
    private $link;

    public function __construct(Item $item)
    {
        $parameters = http_build_query([
            'depart_city_id' => $item->search_id->parameters['source_id'],
            'start_from' => $item->search_id->parameters['start_from'],
            'start_to' => $item->search_id->parameters['start_to'],
            'duration_from' => $item->search_id->parameters['duration_from'],
            'duration_to' => $item->search_id->parameters['duration_to'],
            'adults' => $item->search_id->parameters['adults'],
            'autostart' => 1,
        ]);

        $this->link = 'http://www.onlinetours.ru/hotels/' . $item->hotel->id . ($parameters ? '?' . $parameters : '');

        $this->link = 'https://c43.travelpayouts.com/click?'
            . http_build_query([
                'shmarker' => getenv('TRAVEL_PARTNER_SHMARKER'),
                'promo_id' => getenv('TRAVEL_PARTNER_PROMO_ID'),
                'source_type' => 'customlink',
                'type' => 'click',
                'custom_url' => $this->link,
            ]);
    }

    public function getLink(): string
    {
        return $this->link;
    }
}
