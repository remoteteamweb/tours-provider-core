<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 23:10
 */
namespace TPCore\DataSource\Sources\OnlineToursParser;

use TPCore\DataEntity\Departure\DepartureAbstract;
use TPCore\Tools\Curl;

class Destination
{
    public $destinations = [];

    public function __construct(DepartureAbstract $departure)
    {
        $url = 'https://www.onlinetours.ru/api/v1/popular_suggestions/countries?'
            . 'depart_city_id=' . $departure->getId();

        $curl = new Curl();
        $curl->get($url);

        $this->destinations = $curl->response;
    }
}
