<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-07
 * Time: 23:29
 */
namespace TPCore\DataSource\Sources\OnlineToursParser;

use TPCore\DataEntity\Departure\DepartureInterface;
use  TPCore\DataEntity\Destination\DestinationInterface;
use TPCore\DataSource\SearchAbstract;
use TPCore\Tools\Curl;

class SearchId extends SearchAbstract
{
    private $id;
    private $create_time;
    public $departure;
    public $destination;
    public $parameters;

    public function __construct(
        DestinationInterface $destination,
        DepartureInterface $departure,
        \DateTime $start_from,
        \DateTime $start_to,
        int $adults,
        int $duration_from,
        int $duration_to,
        int $kids,
        CsrfToken $token
    ) {
        $this->destination = $destination;
        $this->departure = $departure;

        $url = 'https://www.onlinetours.ru/api/v1/searches';
        $this->parameters = [
            'adults' => $adults,
            'destination_id' => $destination->id,
            'destination_type' => $destination->type,
            'duration_from' => $duration_from,
            'duration_to' => $duration_to,
            'kids' => $kids,
            'kids_ages' => [],
            'source_id' => $departure->getId(),
            'start_from' => $start_from->format('Y-m-d'),
            'start_to' => $start_to->format('Y-m-d'),
        ];

        $curl = new Curl();
        $curl->setHeaders([
            'accept-encoding' => 'gzip, deflate, br',
            'content-type' => 'application/json;charset=UTF-8',
            'x-csrf-token' => $token,
        ]);
        $curl->post($url, json_encode([
            'search' => $this->parameters,
        ]));
        $curl->close();
        $this->create_time = new \DateTime();

        $data = json_decode(gzdecode($curl->response), true);

        $this->id = $data['id'];

        if (!isset($this->id)) {
            throw new SearchIdException('Не удалось получить search id', 0, null, [
                'curl' => $curl,
            ]);
        }
    }

    public function getAdults()
    {
        return $this->parameters['adults'];
    }

    public function getId()
    {
        $search_info = $this->getInfo();
        $counter = 0;
        while ($search_info['status'] === 'working' && $counter < 10) {
            sleep(3);
            $search_info = $this->getInfo();
            $counter++;
        }

        $current_time = new \DateTime();
        $diff_time = $current_time->getTimestamp() - $this->create_time->getTimestamp();

        if ($diff_time < 10) {
            sleep(10 - $diff_time);
        }

        return $this->id;
    }

    public function __toString(): string
    {
        return $this->getId();
    }

    public function getInfo()
    {
        $url = 'https://www.onlinetours.ru/api/v1/searches/' . $this->id;

        $curl = new Curl();
        $curl->get($url);

        return (array) $curl->response;
    }
}
