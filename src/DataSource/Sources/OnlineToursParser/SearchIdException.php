<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-10
 * Time: 12:46
 */
namespace TPCore\DataSource\Sources\OnlineToursParser;

class SearchIdException extends OnlineToursParserException
{
}
