<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-07
 * Time: 23:49
 */
namespace TPCore\DataSource\Sources\OnlineToursParser;

use Sunra\PhpSimple\HtmlDomParser;
use TPCore\Tools\Curl;

/**
 * DataEntity CsrfToken
 * @package TPCore\DataSource\Sources\OnlineToursParser
 * @psalm-suppress UndefinedDocblockClass
 */
class CsrfToken
{
    private $token;

    /**
     * @psalm-suppress UndefinedDocblockClass
     */
    public function __construct()
    {
        $url = 'https://www.onlinetours.ru';

        try {
            $curl = new Curl();
            $curl->get($url);
            $curl->close();

            /** @psalm-suppress TooManyArguments */
            $dom = HtmlDomParser::str_get_html($curl->response);
            $token = $dom->find('meta[name=csrf-token]', 0)->getAttribute('content');

            $this->token = $token;
        } catch (\Exception $exception) {
            throw new CsrfTokenException(
                'Ошибка получения csrf токена',
                0,
                $exception,
                [
                    'curl' => $curl,
                ]
            );
        }
    }

    public function __toString()
    {
        return $this->token;
    }
}
