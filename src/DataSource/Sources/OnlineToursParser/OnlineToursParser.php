<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-07
 * Time: 16:06
 */
namespace TPCore\DataSource\Sources\OnlineToursParser;

use TPCore\DataEntity\Offer\Hotel;
use TPCore\DataSource\SourceAbstract;
use  TPCore\DataEntity\Offer\Item;
use  TPCore\DataEntity\Offer\ItemList;
use TPCore\DataSource\Sources\OnlineToursParser\DataEntity\HotelLink;
use TPCore\Tools\Curl;

class OnlineToursParser extends SourceAbstract
{
    /**
     * Уникальный идентификатор
     * @var string
     */
    public static $code = 'onlineToursParser';

    public $page;
    public $per_page;
    public $sort;
    /**
     * @var SearchId
     */
    public $search_id;

    public function __construct(SearchId $search_id, string $sort, int $page, int $per_page)
    {
        $this->page = $page;
        $this->sort = $sort;
        $this->per_page = $per_page;
        $this->search_id = $search_id;
    }

    /**
     * Вощвращает уникальный идентификатор DS
     * @return string
     */
    public function getCode(): string
    {
        return self::$code;
    }

    public function request(): Curl
    {
        $url = 'https://www.onlinetours.ru/api/v1/searches/' . $this->search_id . '/results?'
            . 'ticket_strategy=include'
            . '&sort=' . $this->sort
            . '&page=' . $this->page
            . '&per_page=' . $this->per_page;

        $curl = new Curl();
        $curl->setHeader('accept-encoding', 'gzip, deflate, br');
        $curl->get($url);
        $curl->close();

        $curl->response = json_decode(gzdecode($curl->response), true);

        if (empty($curl->response)) {
            throw new OnlineToursParserException(
                'Ошибка запроса',
                0,
                null,
                [
                    'curl' => $curl,
                ]
            );
        }

        return $curl;
    }

    public function getData(): ItemList
    {
        $request = $this->request();
        $data_json = $request->response;

        $result = new ItemList();
        $request_result = $data_json['results'];

        foreach ($request_result as $item) {
            $prepare_data = [
                'id' => $item['cheapestOffer']['id'],
                'startDate' => $item['cheapestOffer']['startDate'],
                'duration' => $item['cheapestOffer']['duration'],
                'roomTypeName' => $item['cheapestOffer']['roomTypeName'],
                'price' => $item['cheapestOffer']['originalPrice'],
                'regularFlight' => $item['cheapestOffer']['regularFlight'],
                'type' => $item['type'],
                'location' => $item['shortLocationCardInfo'],
                'hotel' => [
                    'id' => $item['hotel']['id'],
                    'coords' => $item['hotel']['coords'],
                    'name' => $item['hotel']['name'],
                    'beach' => $item['beachCardInfo'],
                    'stars' => $item['hotel']['stars'],
                    'seoPopularity' => $item['weights']['seoPopularity'],
                    'images' => (array) $item['images'],
                ],
            ];

            try {
                $item = new Item(
                    $prepare_data['id'],
                    $prepare_data['startDate'],
                    $prepare_data['duration'],
                    $prepare_data['roomTypeName'],
                    $prepare_data['price'],
                    $prepare_data['regularFlight'],
                    $prepare_data['type'],
                    $prepare_data['location'],
                    new Hotel(
                        $prepare_data['hotel']['id'],
                        $prepare_data['hotel']['coords'],
                        $prepare_data['hotel']['name'],
                        $prepare_data['hotel']['beach'],
                        $prepare_data['hotel']['stars'],
                        $prepare_data['hotel']['seoPopularity'],
                        $prepare_data['hotel']['images']
                    ),
                    $this->search_id
                );
                $item->setLink(new HotelLink($item));

                $result->append($item);
            } catch (\Exception $ex) {
                $this->addError($ex);
            }
        }

        return $result;
    }
}
