<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-07
 * Time: 16:08
 */
namespace TPCore\DataSource\Sources;

use TPCore\DataEntity\Departure\DepartureInterface;
use TPCore\DataEntity\Destination\DestinationInterface;
use TPCore\DataSource\SearchAbstract;
use TPCore\DataSource\SourceFactoryAbstract;
use TPCore\DataSource\Sources\OnlineToursParser\CsrfToken;
use TPCore\DataSource\Sources\OnlineToursParser\OnlineToursParser;
use TPCore\DataSource\Sources\OnlineToursParser\SearchId;

class OnlineToursParserFactory extends SourceFactoryAbstract
{
    public function createSource(
        SearchAbstract $search_id,
        string $sort = 'cheap',
        int $page = 1,
        int $per_page = 10
    ): OnlineToursParser {
        return new OnlineToursParser(
            $search_id,
            $sort,
            $page,
            $per_page
        );
    }

    public function createSearch(
        DestinationInterface $destination,
        DepartureInterface $departure,
        \DateTime $start_from = null,
        \DateTime $start_to = null,
        int $adults = 2,
        int $duration_from = 5,
        int $duration_to = 25,
        int $kids = 0
    ): SearchId {
        if (is_null($start_from)) {
            $start_from = (new \DateTime())->add(new \DateInterval('P2D'));
        }
        if (is_null($start_to)) {
            $start_to = (clone $start_from)->add(new \DateInterval('P2M'));
        }

        return new SearchId(
            $destination,
            $departure,
            $start_from,
            $start_to,
            $adults,
            $duration_from,
            $duration_to,
            $kids,
            new CsrfToken()
        );
    }
}
