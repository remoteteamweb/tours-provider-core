<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-10
 * Time: 12:35
 */
namespace TPCore\DataSource\Sources;

use TPCore\Exception\Exception;

class OnlineToursParserException extends Exception
{
}
