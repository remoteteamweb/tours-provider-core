<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-06
 * Time: 16:46
 */
namespace TPCore\DataSource;

use TPCore\DataEntity\Departure\DepartureInterface;
use TPCore\DataEntity\Destination\DestinationInterface;

/**
 * DataEntity SourceFactoryAbstract
 * @package TPCore\DataSource
 */
abstract class SourceFactoryAbstract
{
    /**
     * @param DestinationInterface $destination
     * @param DepartureInterface $departure
     * @param \DateTime $start_from
     * @param \DateTime $start_to
     * @param int $adults
     * @param int $duration_from
     * @param int $duration_to
     * @param int $kids
     * @return SearchAbstract
     */
    abstract public function createSearch(
        DestinationInterface $destination,
        DepartureInterface $departure,
        \DateTime $start_from,
        \DateTime $start_to,
        int $adults,
        int $duration_from,
        int $duration_to,
        int $kids
    );

    /**
     * @param SearchAbstract $search_id
     * @param string $sort
     * @param int $page
     * @param int $per_page
     * @return SourceAbstract
     */
    abstract public function createSource(
        SearchAbstract $search_id,
        string $sort,
        int $page,
        int $per_page
    );
}
