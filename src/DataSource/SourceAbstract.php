<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-06
 * Time: 16:46
 */
namespace TPCore\DataSource;

use TPCore\DataEntity\Offer\ItemList;

/**
 * DataEntity SourceAbstract
 * @package TPCore\DataSource
 */
abstract class SourceAbstract
{
    protected $errors = [];

    abstract public function getData():ItemList;

    public function addError(\Throwable $error)
    {
        $this->errors[] = $error;
    }

    public function getErrors()
    {
        return $this->errors;
    }
}
