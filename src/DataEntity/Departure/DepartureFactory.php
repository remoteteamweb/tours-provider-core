<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 18:22
 */
namespace TPCore\DataEntity\Departure;

class DepartureFactory
{
    protected $ids = [];

    public function __construct()
    {
        $this->ids = [
            Kazan::$ID => Kazan::class,
        ];
    }

    public function createById($id)
    {
        if (!isset($this->ids[$id])) {
            throw new \InvalidArgumentException('Не найден город с id: ' . $id);
        }
        $class_name = $this->ids[$id];

        return new $class_name;
    }
}
