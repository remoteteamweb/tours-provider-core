<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-08
 * Time: 21:52
 */
namespace  TPCore\DataEntity\Departure;

interface DepartureInterface
{
    public function getId():int;

    public function getName():string;

    public function __toString():string;
}
