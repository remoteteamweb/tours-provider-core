<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-08
 * Time: 23:26
 */
namespace  TPCore\DataEntity\Departure;

abstract class DepartureAbstract implements DepartureInterface
{
    public $name;

    public function getName(): string
    {
        return $this->name;
    }

    public function __toString():string
    {
        return (string) $this->getId();
    }
}
