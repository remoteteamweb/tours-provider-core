<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 18:02
 */
namespace TPCore\DataEntity\Departure;

class Kazan extends DepartureAbstract
{
    /**
     * @var int
     */
    public static $ID = 10688;
    public $name = 'Казань';

    public function getId():int
    {
        return static::$ID;
    }
}
