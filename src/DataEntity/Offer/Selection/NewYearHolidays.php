<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-13
 * Time: 12:37
 */
namespace TPCore\DataEntity\Offer\Selection;

use TPCore\DataEntity\Departure\DepartureInterface;
use TPCore\DataEntity\Destination\DestinationInterface;
use TPCore\DataEntity\Offer\ItemList;
use TPCore\DataSource\DataSourceFactory;
use TPCore\DataSource\Sources\OnlineToursParser\OnlineToursParser;

class NewYearHolidays extends ItemList
{
    public function __construct(
        DepartureInterface $departure,
        array $destinations,
        int $duration_from = 5,
        int $duration_to = 12,
        int $adults = 2,
        int $kids = 0
    ) {
        $ds_factory = new DataSourceFactory();
        $source_factory = $ds_factory->create();

        $current_date = new \DateTime();
        $start_from = (new \DateTime())->setDate(
            (int) $current_date->format('Y'),
            12,
            23
        );
        $start_to = (new \DateTime())->setDate(
            ((int) $current_date->format('Y')) + 1,
            1,
            3
        );

        foreach ($destinations as $destination) {
            /* @var $destination DestinationInterface */
            /* @var $data_source OnlineToursParser */

            $search = $source_factory->createSearch(
                $destination,
                $departure,
                $start_from,
                $start_to,
                $adults,
                $duration_from,
                $duration_to,
                $kids
            );
            $data_source = $source_factory->createSource($search);
            $offers = $data_source->getData();

            if (count($offers) <= 0) {
                continue;
            }

            /*
             * Берем самое дешевое предложение
             */
            $this->append($offers[0]);
        }
    }
}
