<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-08
 * Time: 22:43
 */
namespace  TPCore\DataEntity\Offer;

class Hotel
{
    public $id;
    public $coords;
    public $name;
    public $beach;
    public $stars;
    public $seoPopularity;
    public $images;

    public function __construct(
        int $id,
        $coords,
        string $name,
        $beach,
        $stars,
        $seoPopularity,
        array $images
    ) {
        $this->id = $id;
        $this->coords = $coords;
        $this->name = $name;
        $this->beach = $beach;
        $this->stars = $stars;
        $this->seoPopularity = $seoPopularity;
        $this->images = $images;
    }
}
