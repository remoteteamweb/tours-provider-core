<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-08
 * Time: 22:34
 */
namespace  TPCore\DataEntity\Offer;

use TPCore\DataEntity\ItemLinkAbstract;
use TPCore\DataEntity\ItemLinkSetterInterface;
use TPCore\DataSource\SearchAbstract;

class Item implements ItemLinkSetterInterface
{
    public $id;
    public $date;
    public $duration;
    public $roomType;
    public $price;
    public $regularFlight;
    public $type;
    public $location;
    public $hotel;
    public $search_id;
    public $link;

    public function __construct(
        string $id,
        string $date,
        int $duration,
        string $roomType,
        int $price,
        bool $regularFlight,
        string $type,
        string $location,
        Hotel $hotel,
        SearchAbstract $search_id
    ) {
        if ($type !== 'Hotel') {
            throw new \InvalidArgumentException('type !== Hotel');
        }

        $this->id = $id;
        $this->date = new \DateTime($date);
        $this->duration = $duration;
        $this->roomType = $roomType;
        $this->price = $price;
        $this->regularFlight = $regularFlight;
        $this->type = $type;
        $this->location = $location;
        $this->hotel = $hotel;
        $this->search_id = $search_id;
    }

    public function setLink(ItemLinkAbstract $link)
    {
        $this->link = $link;
    }
}
