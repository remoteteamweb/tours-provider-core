<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-10
 * Time: 18:58
 */
namespace TPCore\DataEntity\Offer;

use morphos\MorphosTwigExtension;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class ViewAbstract
{
    protected $data;
    protected $environment;

    public function __construct($data)
    {
        $this->data = $data;

        $loader = new FilesystemLoader(__DIR__ . DIRECTORY_SEPARATOR . 'Views');

        $this->environment = new Environment($loader);
        $this->environment->addExtension(new MorphosTwigExtension());
    }

    public function __toString():string
    {
        return $this->environment->render(
            (new \ReflectionClass($this))->getShortName() . '.twig',
            [
                'data' => $this->data,
            ]
        );
    }

    /**
     * @param $item
     * @return string
     * @deprecated
     */
    public function getOfferLink($item)
    {
        $parameters = http_build_query([
            'depart_city_id' => $item->search_id->parameters['source_id'],
            'start_from' => $item->search_id->parameters['start_from'],
            'start_to' => $item->search_id->parameters['start_to'],
            'duration_from' => $item->search_id->parameters['duration_from'],
            'duration_to' => $item->search_id->parameters['duration_to'],
            'autostart' => 1,
        ]);

        $url = 'http://www.onlinetours.ru/hotels/' . $item->hotel->id . ($parameters ? '?' . $parameters : '');

        $partner_link = 'https://c43.travelpayouts.com/click?'
            . http_build_query([
                'shmarker' => getenv('TRAVEL_PARTNER_SHMARKER'),
                'promo_id' => getenv('TRAVEL_PARTNER_PROMO_ID'),
                'source_type' => 'customlink',
                'type' => 'click',
                'custom_url' => $url,
            ]);

        return $partner_link;
    }
}
