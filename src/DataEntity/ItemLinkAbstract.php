<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-15
 * Time: 08:08
 */
namespace TPCore\DataEntity;

abstract class ItemLinkAbstract
{
    abstract public function getLink():string;

    public function __toString():string
    {
        return $this->getLink();
    }
}
