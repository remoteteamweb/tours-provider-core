<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-15
 * Time: 08:12
 */
namespace TPCore\DataEntity;

interface ItemLinkSetterInterface
{
    public function setLink(ItemLinkAbstract $link);
}
