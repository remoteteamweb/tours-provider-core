<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-08
 * Time: 21:43
 */
namespace  TPCore\DataEntity\Destination;

class UAE extends DestinationAbstract
{
    public static $ID = 1643;

    /** @var int */
    public $id = 1643;
    public $visaRequired = false;
    /** @var string */
    public $type = 'Country';
    public $name = 'Арабские Эмираты';
}
