<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 22:57
 */
namespace TPCore\DataEntity\Destination;

class India extends DestinationAbstract
{
    public static $ID = 303;
    /** @var int */
    public $id = 303;
    public $visaRequired = true;
    /** @var string */
    public $type = 'Country';
    public $name = 'Индия';
}
