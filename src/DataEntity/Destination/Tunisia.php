<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 22:57
 */
namespace TPCore\DataEntity\Destination;

class Tunisia extends DestinationAbstract
{
    public static $ID = 1577;

    /** @var int */
    public $id = 1577;
    public $visaRequired = false;
    /** @var string */
    public $type = 'Country';
    public $name = 'Тунис';
}
