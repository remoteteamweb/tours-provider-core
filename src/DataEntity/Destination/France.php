<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 22:57
 */
namespace TPCore\DataEntity\Destination;

class France extends DestinationAbstract
{
    public static $ID = 141;
    /** @var int */
    public $id = 141;
    public $visaRequired = true;
    /** @var string */
    public $type = 'Country';
    public $name = 'Франция';
}
