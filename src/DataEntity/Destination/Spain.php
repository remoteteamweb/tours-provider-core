<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 22:57
 */
namespace TPCore\DataEntity\Destination;

class Spain extends DestinationAbstract
{
    public static $ID = 1457;

    /** @var int */
    public $id = 1457;
    public $visaRequired = true;
    /** @var string */
    public $type = 'Country';
    public $name = 'Испания';
}
