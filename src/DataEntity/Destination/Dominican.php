<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 22:57
 */
namespace TPCore\DataEntity\Destination;

class Dominican extends DestinationAbstract
{
    public static $ID = 2;
    /** @var int */
    public $id = 2;
    public $visaRequired = false;
    /** @var string */
    public $type = 'Country';
    public $name = 'Болгария';
}
