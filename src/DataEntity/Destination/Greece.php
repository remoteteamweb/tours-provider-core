<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-08
 * Time: 21:51
 */
namespace  TPCore\DataEntity\Destination;

class Greece extends DestinationAbstract
{
    public static $ID = 239;
    /** @var int */
    public $id = 239;
    public $visaRequired = true;
    /** @var string */
    public $type = 'Country';
    public $name = 'Греция';
}
