<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 22:57
 */
namespace TPCore\DataEntity\Destination;

class China extends DestinationAbstract
{
    public static $ID = 4219;
    /** @var int */
    public $id = 4219;
    public $visaRequired = true;
    /** @var string */
    public $type = 'Country';
    public $name = 'Израиль';
}
