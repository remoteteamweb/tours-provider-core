<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-08
 * Time: 21:43
 */
namespace  TPCore\DataEntity\Destination;

class Cyprus extends DestinationAbstract
{
    public static $ID = 4203;
    /** @var int */
    public $id = 4203;
    public $visaRequired = false;
    /** @var string */
    public $type = 'Country';
    public $name = 'Кипр';
}
