<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 18:22
 */
namespace TPCore\DataEntity\Destination;

class DestinationFactory
{
    protected $all = [];
    protected $other = [];

    public function __construct()
    {
        $this->all = [
            Abkhazia::$ID => new Abkhazia(),
            Andorra::$ID => new Andorra(),
            Bulgaria::$ID => new Bulgaria(),
            China::$ID => new China(),
            Cuba::$ID => new Cuba(),
            Cyprus::$ID => new Cyprus(),
            Czech::$ID => new Czech(),
            Dominican::$ID => new Dominican(),
            Egypt::$ID => new Egypt(),
            France::$ID => new France(),
            Greece::$ID => new Greece(),
            Hungary::$ID => new Hungary(),
            India::$ID => new India(),
            Indonesia::$ID => new Indonesia(),
            Israel::$ID => new Israel(),
            Italy::$ID => new Italy(),
            Jordan::$ID => new Jordan(),
            Maldives::$ID => new Maldives(),
            Malta::$ID => new Malta(),
            Mexico::$ID => new Mexico(),
            Montenegro::$ID => new Montenegro(),
            Spain::$ID => new Spain(),
            SriLanka::$ID => new SriLanka(),
            Tanzania::$ID => new Tanzania(),
            Thailand::$ID => new Thailand(),
            Tunisia::$ID => new Tunisia(),
            Turkey::$ID => new Turkey(),
            UAE::$ID => new UAE(),
            Vietnam::$ID => new Vietnam(),
        ];
    }

    public function createById($id)
    {
        $all = $this->all + $this->other;

        if (!isset($all[$id])) {
            throw new \InvalidArgumentException('Не найдено направление с id: ' . $id);
        }

        return $all[$id];
    }

    public function createAllPopular()
    {
        return [
            new Turkey(),
            new UAE(),
            new Tunisia(),
            new Greece(),
            new India(),
            new Cyprus(),
            new Thailand(),
        ];
    }

    public function createAll()
    {
        return [
            new Abkhazia(),
            new Andorra(),
            new Bulgaria(),
            new China(),
            new Cuba(),
            new Cyprus(),
            new Czech(),
            new Dominican(),
            new Egypt(),
            new France(),
            new Greece(),
            new Hungary(),
            new India(),
            new Indonesia(),
            new Israel(),
            new Italy(),
            new Jordan(),
            new Maldives(),
            new Malta(),
            new Mexico(),
            new Montenegro(),
            new Spain(),
            new SriLanka(),
            new Tanzania(),
            new Thailand(),
            new Tunisia(),
            new Turkey(),
            new UAE(),
            new Vietnam(),
        ];
    }
}
