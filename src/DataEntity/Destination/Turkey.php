<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-08
 * Time: 21:43
 */
namespace  TPCore\DataEntity\Destination;

class Turkey extends DestinationAbstract
{
    public static $ID = 1601;

    /** @var int */
    public $id = 1601;
    public $visaRequired = false;
    /** @var string */
    public $type = 'Country';
    public $name = 'Турция';
}
