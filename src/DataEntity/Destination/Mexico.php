<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 22:57
 */
namespace TPCore\DataEntity\Destination;

class Mexico extends DestinationAbstract
{
    public static $ID = 96445;

    /** @var int */
    public $id = 96445;
    public $visaRequired = true;
    /** @var string */
    public $type = 'Country';
    public $name = 'Мексика';
}
