<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 22:57
 */
namespace TPCore\DataEntity\Destination;

class Abkhazia extends DestinationAbstract
{
    public static $ID = 64464;
    /** @var int */
    public $id = 64464;
    public $visaRequired = false;
    /** @var string */
    public $type = 'Country';
    public $name = 'Абхазия';
}
