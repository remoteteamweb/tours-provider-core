<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 22:57
 */
namespace TPCore\DataEntity\Destination;

class Hungary extends DestinationAbstract
{
    public static $ID = 290;
    /** @var int */
    public $id = 290;
    public $visaRequired = true;
    /** @var string */
    public $type = 'Country';
    public $name = 'Венгрия';
}
