<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 22:57
 */
namespace TPCore\DataEntity\Destination;

class Vietnam extends DestinationAbstract
{
    public static $ID = 1697;

    /** @var int */
    public $id = 1697;
    public $visaRequired = false;
    /** @var string */
    public $type = 'Country';
    public $name = 'Вьетнам';
}
