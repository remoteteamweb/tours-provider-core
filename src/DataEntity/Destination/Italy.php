<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 22:57
 */
namespace TPCore\DataEntity\Destination;

class Italy extends DestinationAbstract
{
    public static $ID = 352;

    /** @var int */
    public $id = 352;
    public $visaRequired = true;
    /** @var string */
    public $type = 'Country';
    public $name = 'Италия';
}
