<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 22:57
 */
namespace TPCore\DataEntity\Destination;

class SriLanka extends DestinationAbstract
{
    public static $ID = 1477;

    /** @var int */
    public $id = 1477;
    public $visaRequired = true;
    /** @var string */
    public $type = 'Country';
    public $name = 'Шри-Ланка';
}
