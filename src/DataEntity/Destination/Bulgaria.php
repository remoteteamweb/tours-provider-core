<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 22:57
 */
namespace TPCore\DataEntity\Destination;

class Bulgaria extends DestinationAbstract
{
    public static $ID = 4201;
    /** @var int */
    public $id = 4201;
    public $visaRequired = true;
    /** @var string */
    public $type = 'Country';
    public $name = 'Болгария';
}
