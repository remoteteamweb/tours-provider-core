<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-08
 * Time: 23:26
 */
namespace  TPCore\DataEntity\Destination;

abstract class DestinationAbstract implements DestinationInterface
{
    public $name;

    public function getName():string
    {
        return $this->name;
    }
}
