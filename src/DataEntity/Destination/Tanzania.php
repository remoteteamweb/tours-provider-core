<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 22:57
 */
namespace TPCore\DataEntity\Destination;

class Tanzania extends DestinationAbstract
{
    public static $ID = 1520;

    /** @var int */
    public $id = 1520;
    public $visaRequired = false;
    /** @var string */
    public $type = 'Country';
    public $name = 'Танзания';
}
