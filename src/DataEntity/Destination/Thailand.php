<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-08
 * Time: 21:43
 */
namespace  TPCore\DataEntity\Destination;

class Thailand extends DestinationAbstract
{
    /** @var int */
    public static $ID = 1558;
    /** @var int */
    public $id = 1558;
    public $visaRequired = false;
    /** @var string */
    public $type = 'Country';
    public $name = 'Тайланд';
}
