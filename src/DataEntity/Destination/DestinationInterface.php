<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-08
 * Time: 21:52
 */
namespace  TPCore\DataEntity\Destination;

interface DestinationInterface
{
    public function getName(): string;
}
