<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 22:57
 */
namespace TPCore\DataEntity\Destination;

class Cuba extends DestinationAbstract
{
    public static $ID = 4214;
    /** @var int */
    public $id = 4214;
    public $visaRequired = false;
    /** @var string */
    public $type = 'Country';
    public $name = 'Куба';
}
