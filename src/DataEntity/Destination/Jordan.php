<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-11
 * Time: 22:57
 */
namespace TPCore\DataEntity\Destination;

class Jordan extends DestinationAbstract
{
    public static $ID = 397;

    /** @var int */
    public $id = 397;
    public $visaRequired = false;
    /** @var string */
    public $type = 'Country';
    public $name = 'Иордания';
}
