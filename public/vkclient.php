<?php
/*
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-10
 * Time: 13:39
 */

use TPCore\DataEntity\Departure\Kazan;
use TPCore\DataEntity\Destination\Turkey;
use TPCore\DataEntity\Offer\Views\OfferItem;
use TPCore\DataSource\DataSourceFactory;
use TPCore\Tools\ImageLoader;
use TPCore\Tools\Path;
use TPCore\Tools\Vk\AccessToken;
use TPCore\Tools\Vk\WallPhoto;
use VK\Client\Enums\VKLanguage;
use VK\Client\VKApiClient;

require './../bootstrap.php';

$ds_factory = new DataSourceFactory();
$source_factory = $ds_factory->create();
$vk_client = new VKApiClient('5.101', VKLanguage::RUSSIAN);
$group_id = (int) getenv('GROUP_ID');
$user_token = new AccessToken(
    getenv('ACCESS_TOKEN_GROUP_ADMIN')
);

$search = $source_factory->createSearch(new Turkey(), new Kazan());
$data_source = $source_factory->createSource($search);

$offers = $data_source->getData();

$best_offer = $offers[0];

$images = new ImageLoader((array) $best_offer->hotel->images, Path::tmp());
$wall_photos = new WallPhoto($vk_client, $user_token, $group_id, $images->getFiles());

$res = $vk_client->wall()->post($user_token, [
    'owner_id' => "-$group_id",
    'from_group' => 1,
    'signed' => 0,
    'publish_date' => (new DateTime())->add(new \DateInterval('P2D'))->getTimestamp(),
    'message' => (string) (new OfferItem($best_offer)),
    'attachments' => $wall_photos->getAttachmentsString(),
]);
