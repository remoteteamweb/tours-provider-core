<?php
/**
 * Created by PhpStorm.
 * User: valentinkruglikov
 * Date: 2019-08-06
 * Time: 16:43
 */
use TPCore\DataEntity\Departure\Kazan;
use TPCore\DataEntity\Destination\DestinationFactory;
use TPCore\DataEntity\Destination\Thailand;
use TPCore\DataEntity\Offer\ItemList;
use TPCore\DataEntity\Offer\Views\CountryList;
use TPCore\DataSource\DataSourceFactory;
use TPCore\DataSource\Sources\OnlineToursParserFactory;

require './../bootstrap.php';

$ds_factory = new DataSourceFactory();
/** @var $source_factory OnlineToursParserFactory */
$source_factory = $ds_factory->create();
$destination_factory = new DestinationFactory();

$all_destination = [new Thailand()];

$departure = new Kazan();
$all_searches = array_map(function ($destination) use ($source_factory, $departure) {
    return $search = $source_factory->createSearch(
        $destination,
        $departure
    );
    sleep(1);
}, $all_destination);
sleep(10);

$best_offers = new ItemList();
foreach ($all_searches as $search) {
    $data_source = $source_factory->createSource(
        $search,
        'cheap',
        1,
        1
    );
    $offers = $data_source->getData();

    if (!isset($offers[0])) {
        continue;
    }

    $offer = $offers[0];
    $best_offers->append($offer);
}

print new CountryList($best_offers);
